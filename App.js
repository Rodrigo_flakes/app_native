import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, FlatList, ActivityIndicator, Button } from 'react-native';
import { fetchOrders, sendFeedback } from './api';

export default function App() {
  const [orders, setOrders] = useState([]);
  const [selectedOrder, setSelectedOrder] = useState(null);

  useEffect(() => {
    // Вызов функции для получения списка заказов
    fetchOrders()
      .then((orders) => setOrders(orders))
      .catch((error) => console.error('Ошибка при получении списка заказов:', error));
  }, []);

  const handleOrderPress = (order) => {
    setSelectedOrder(order);
  };

  const handleBackPress = () => {
    setSelectedOrder(null);
  };

  const renderItem = ({ item }) => (
    <TouchableOpacity style={styles.orderCard} onPress={() => handleOrderPress(item)}>
      <Text style={styles.orderText}>{item}</Text>
    </TouchableOpacity>
  );

  const handleSendFeedback = () => {
    const feedbackData = {
      rating: 4,
      dislikes: ['Долгая доставка', 'Плохое качество'],
      likes: ['Быстрая доставка', 'Отличное качество'],
    };

    sendFeedback(feedbackData)
      .then((response) => {
        console.log('Обратная связь отправлена успешно:', response);
        // Выполните какие-либо действия после успешной отправки обратной связи
      })
      .catch((error) => console.error('Ошибка при отправке обратной связи:', error));
  };

  return (
    <View style={styles.container}>
      {!selectedOrder ? (
        <>
          <Text style={styles.title}>Список заказов</Text>
          <FlatList
            data={orders}
            renderItem={renderItem}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={styles.ordersList}
          />
        </>
      ) : (
        <RatingScreen order={selectedOrder} onBackPress={handleBackPress} handleSendFeedback={handleSendFeedback} />
      )}
    </View>
  );
}

const RatingScreen = ({ order, onBackPress, handleSendFeedback }) => {
  const [rating, setRating] = useState(0);
  const [likes, setLikes] = useState([]);
  const [dislikes, setDislikes] = useState([]);
  const [selectedDislike, setSelectedDislike] = useState(null);
  const [loading, setLoading] = useState(true);
  const [showDislikeMessage, setShowDislikeMessage] = useState(false);

  useEffect(() => {
    // Симуляция загрузки данных с сервера
    setTimeout(() => {
      if (rating <= 3) {
        setDislikes(['Долгая доставка', 'Плохое качество', 'Невежливый курьер']);
        setShowDislikeMessage(true);
      } else {
        setLikes(['Быстрая доставка', 'Отличное качество', 'Вежливый курьер']);
        setShowDislikeMessage(false);
      }
      setLoading(false);
    }, 2000);
  }, [rating]);

  const handleRatingChange = (value) => {
    setRating(value);
  };

  const handleDislikePress = (dislike) => {
    setSelectedDislike(dislike);
  };

  const renderLikesDislikes = () => {
    if (loading) {
      return <ActivityIndicator size="small" color="#888" />;
    }

    if (rating <= 3) {
      return (
        <>
          <Text style={styles.sectionTitle}>Не понравилось:</Text>
          {dislikes.map((item, index) => (
            <TouchableOpacity
              key={index}
              style={[styles.optionButton, selectedDislike === item && styles.optionButtonSelected]}
              onPress={() => handleDislikePress(item)}
            >
              <Text style={styles.optionButtonText}>{item}</Text>
            </TouchableOpacity>
          ))}
        </>
      );
    } else {
      return (
        <>
          <Text style={styles.sectionTitle}>Понравилось:</Text>
          {likes.map((item, index) => (
            <Text key={index}>{item}</Text>
          ))}
        </>
      );
    }
  };

  return (
    <View style={styles.ratingContainer}>
      <TouchableOpacity style={styles.backButton} onPress={onBackPress}>
        <Text style={styles.backButtonText}>Назад</Text>
      </TouchableOpacity>
      <Text style={styles.title}>Оценка заказа: {order}</Text>
      <Text style={styles.ratingTitle}>Поставьте оценку:</Text>
      <View style={styles.ratingContainer}>
        {[1, 2, 3, 4, 5].map((value) => (
          <TouchableOpacity
            key={value}
            style={[styles.ratingButton, rating >= value && styles.ratingButtonSelected]}
            onPress={() => handleRatingChange(value)}
          >
            <Text style={styles.ratingText}>{value}</Text>
          </TouchableOpacity>
        ))}
      </View>
      <View style={styles.likesDislikesContainer}>{renderLikesDislikes()}</View>
      {showDislikeMessage && rating <= 3 && (
        <Text style={styles.dislikeMessage}>Сообщение о неприязни появляется здесь</Text>
      )}
      <Button title="Отправить" onPress={handleSendFeedback} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  ordersList: {
    flexGrow: 1,
    justifyContent: 'center',
  },
  orderCard: {
    height: 100,
    backgroundColor: '#f2f2f2',
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  orderText: {
    fontSize: 20,
  },
  ratingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  backButton: {
    position: 'absolute',
    top: 20,
    left: 20,
    padding: 10,
  },
  backButtonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#333',
  },
  ratingTitle: {
    fontSize: 20,
    marginBottom: 20,
  },
  ratingButton: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: '#f2f2f2',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 5,
  },
  ratingButtonSelected: {
    backgroundColor: '#e2e2e2',
  },
  ratingText: {
    fontSize: 18,
  },
  sectionTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  likesDislikesContainer: {
    marginTop: 20,
  },
  optionButton: {
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#f2f2f2',
    marginBottom: 10,
  },
  optionButtonSelected: {
    backgroundColor: '#e2e2e2',
  },
  optionButtonText: {
    fontSize: 16,
  },
  dislikeMessage: {
    fontSize: 16,
    marginTop: 10,
    color: 'red',
  },
});
