import axios from 'axios';

// Функция для получения списка заказов
export const fetchOrders = async () => {
  try {
    const response = await axios.get('https://example.com/orders/');
    const orders = response.data;
    console.log('Список заказов:', orders);
    // Здесь вы можете обновить состояние вашего приложения с помощью полученных данных
    return orders;
  } catch (error) {
    console.error('Ошибка при получении списка заказов:', error);
    throw error;
  }
};

// Функция для получения списка понравившихся/непонравившихся вещей
export const fetchCriterias = async () => {
  try {
    const response = await axios.get('https://example.com/criterias/');
    const criterias = response.data;
    console.log('Список понравившихся/непонравившихся вещей:', criterias);
    // Здесь вы можете обновить состояние вашего приложения с помощью полученных данных
    return criterias;
  } catch (error) {
    console.error('Ошибка при получении списка понравившихся/непонравившихся вещей:', error);
    throw error;
  }
};

// Функция для отправки обратной связи
export const sendFeedback = async (feedback) => {
  try {
    const response = await axios.post('https://example.com/feedback', feedback);
    console.log('Обратная связь отправлена успешно:', response.data);
    // Здесь вы можете выполнить какие-либо действия после успешной отправки обратной связи
    return response.data;
  } catch (error) {
    console.error('Ошибка при отправке обратной связи:', error);
    throw error;
  }
};
